function sort_by_priority_desc(first, second) {
  return second.priority - first.priority;
}

(function task_getter() {
  var jqxhr = $.getJSON("/tasks?json", function(tasks) {
        tasks.sort(sort_by_priority_desc);
        var task_list = $("#task-list");
        task_list.empty();
        $.each(tasks, function(index, task) {
            task_list.append("<li>"+"["+task.priority+"] "+task.name+" ("+task.description+")</li>")
        });
  })
  .done(function() {
      setTimeout(task_getter, 5000);
  });
})();