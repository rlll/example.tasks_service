from .tasks_service import app, storage
import click


@click.command()
@click.option('--port', default=3000, help="Listen port number.")
@click.option('--path', default=None, help="Path to storage file")
def run(port, path):
    """Tasks service"""

    storage.path = path
    print("Storage:", storage.path)
    app.run(port=port)
