"""
    Tasks service application
"""

import os
import anyconfig
from flask import Flask, jsonify, request, render_template

from .storage import Storage

_basepath = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

_requests_schemas = {
    "task-add": anyconfig.load(os.path.join(_basepath, "schemas", "task-add-request.json"))
}

storage = Storage()


@app.errorhandler(404)
def not_found(error):
    return jsonify({"error": "Not found"}), 404


@app.route("/tasks", methods=["GET"])
def tasks_list():
    """
        Getting task list
        If request has parameter json like /tasks?json response in json format else in html
    """
    is_json = request.args.get("json", None) is not None
    if is_json:
        return jsonify(storage.list_tasks()), 200
    else:
        return render_template("tasks.html")


@app.route("/tasks", methods=["POST"])
def task_add():
    """ Adding new task """
    request_data = request.get_json()
    if request_data is None:
        return jsonify({"error": "Bad Request", "description": "Non JSON message"}), 400
    schema = _requests_schemas["task-add"]
    (validate_result, validate_error) = anyconfig.validate(request_data, schema=schema)
    if not validate_result:
        return jsonify({"error": "Bad Request", "description": "Non valid message format"}), 400

    key = storage.add_task(
        name=request_data["name"],
        description=request_data["description"] if "description" in request_data else "",
        priority=request_data["priority"] if "priority" in request_data else 0
    )

    return jsonify({"key": key}), 200
