"""
    Data storage
"""

import os
import json
import uuid


class Storage:
    """ Store data in json file """

    def __init__(self, path=None):
        if path is None:
            self._path = os.path.join(os.path.expanduser("~"), ".tasks_service")
        else:
            self._path = os.path.abspath(path)

        self._filename = os.path.join(self._path, "storage.json")

        self._tasks = {}

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        if path is None:
            self._path = os.path.join(os.path.expanduser("~"), ".tasks_service")
        else:
            self._path = os.path.abspath(path)
        self._filename = os.path.join(self._path, "storage.json")

    @property
    def filename(self):
        return self._filename

    def _load(self):
        if not os.path.exists(self._filename):
            if not os.path.exists(self._path):
                os.makedirs(self._path)
            content = {"tasks": {}}
        else:
            content = json.load(open(self._filename, encoding="utf-8"))
        if "tasks" in content:
            self._tasks = content["tasks"]
        else:
            raise Exception("Incorrect storage file '{path}'".format(self._filename))

    def _save(self):
        if not os.path.exists(self._path):
            os.makedirs(self._path)
        content = {"tasks": self._tasks}
        json.dump(content, open(self._filename, "w", encoding="utf-8"), ensure_ascii=False)

    def add_task(self, name, description="", priority=0):
        self._load()
        key = str(uuid.uuid4())
        self._tasks[key] = {
            "priority": priority,
            "name": name,
            "description": description
        }
        self._save()
        return key

    def remove_task(self, key):
        self._load()
        if key in self._tasks:
            del self._tasks[key]
        self._save()

    def list_tasks(self):
        self._load()
        tasks = []
        for key, task in self._tasks.items():
            task["_key"] = key
            tasks.append(task)
        return tasks


if __name__ == "__main__":
    s = Storage()
    print(s.list_tasks())