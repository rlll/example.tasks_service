import os
import sys

py_version = sys.version_info[:2]

if (3, 0) < py_version < (3, 2):
    raise RuntimeError("Application requires Python 3.2 or later")

requires = [
    "anyconfig >= 0.6.0",
    "Flask >= 0.11.1",
]

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
version_txt = os.path.join(here, "tasks_service/version.txt")
app_version = open(version_txt).read().strip()

dist = setup(
    name="tasks_service",
    version=app_version,
    license="None",
    url="",
    description="Tasks service",
    long_description="Tasks service",
    classifiers="",
    author="rlll",
    author_email="rlllx33@gmail.com",
    packages=find_packages(),
    install_requires=requires,
    include_package_data=True,
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "tasks = tasks_service.run:run"
        ],
    },
)
